package com.zuitt.example;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int num = 0;

        System.out.println("Input an integer whose factorial will be computed: ");
        try {
            long answer = 1;
            num = in.nextInt();

            if (num==1){
                System.out.println("The factorial of 1 is 1");
                return;
            } else if (num==0) {
                System.out.println("The factorial of 0 is 1");
                return;
            } else if (num<1) {
                System.out.println("Invalid input. Please enter a valid number.");
                return;
            }
            else{
                for (int counter = 1; counter<=num; counter++ ){
                    answer *= counter;
                }
                System.out.println("The factorial of "+num + " is "+answer);
            }

        }
        catch (Exception e){
            System.out.println("Invalid input. Please enter a valid number.");
            e.printStackTrace();
        }
        finally {
            System.out.println("You entered: "+num);
        }


    }

}
